// C program to solve Advent of Code - Day 05 - Problem 2
//
// After the rearrangement procedure completes, what crate ends up on top of each stack?
//
// https://adventofcode.com/2022/day/5
//
//


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 1024


void slice(const char *str, char *result, size_t start, size_t end) {
    // taken from http://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing
    strncpy(result, str + start, end - start);
}


const char *pop_n(char stack[MAX], int n) {
    char *items_ptr = (char*) calloc(n+1, sizeof(char));
    int stack_length = strlen(stack);
    int start = stack_length - n;
    int end = stack_length;

    slice(stack, items_ptr, start, end);
    stack[strlen(stack) - n] = '\0';

    // TODO: Where and how should free(items_ptr) be called?

    return items_ptr;
}


int push_n(char stack[MAX], char items[]) {
    strcat(stack, items);
    return 0;
}


int apply_task(char stacks[][MAX], char task[MAX]) {
    // ideas from https://c-for-dummies.com/blog/?p=1769

    char *task_ptr = strdup(task); // for integer casting
    char *end_ptr;  // for string to integer casting

    char times[MAX];
    char from[MAX];
    char to[MAX];

    // split string task by ' '
    strsep(&task_ptr," ");  // = 'move'
    strcpy(times, strsep(&task_ptr," "));
    strsep(&task_ptr," ");  // = 'from'
    strcpy(from, strsep(&task_ptr," "));
    strsep(&task_ptr," ");  // = 'to'
    strcpy(to, strsep(&task_ptr," "));

    // Cast strings to integers
    int int_times = strtol(times, &end_ptr, 10);
    int int_from = strtol(from, &end_ptr, 10);
    int int_to = strtol(to, &end_ptr, 10);

    char *items_ptr = (char*) calloc(int_times+1, sizeof(char));

    // Call pop_n and push_n
    strcpy(items_ptr, pop_n(stacks[int_from-1], int_times));
    push_n(stacks[int_to-1], items_ptr);

    free(items_ptr);

    return 0;
}


int test_apply_task() {
    char stacks[2][MAX] = {
        "12345",
        "abcde",
    };

    char task[MAX] = "move 3 from 2 to 1\n";
    apply_task(stacks, task);

    printf("Result:\n%s\n%s\n", stacks[0], stacks[1]);

    return 0;
}


/// Main Function ///


int main() {

    int j_max;

    char line[MAX];
    char task[MAX];
    FILE *file_ptr = fopen("day05/input.txt", "r");

    int num_of_stacks = 0;
    int stacks_declared = 0;
    int stacks_initialized = 0;

    char item[2] = "\0";

    num_of_stacks = 9;  // TODO: It's hardcoded :(

    char reverse_stacks[num_of_stacks][MAX];
    char stacks[num_of_stacks][MAX];

    // First, initialize stacks as emtpy
    // TODO: find a better solution
    for(int i = 0; i < num_of_stacks; i++) {
        reverse_stacks[i][0] = '\0';
        stacks[i][0] = '\0';
    }

    // Build initial stacks
    while (fgets(line, MAX, file_ptr) != NULL) {
        // Build invere stacks via push
        for (int i = 0; i < num_of_stacks; i++) {
            item[0] = line[4*i + 1];
            if (item[0] == '1') {
                stacks_initialized++;
            }
            if (item[0] != ' ' && stacks_initialized == 0) {
                push_n(reverse_stacks[i], item);
            }
        }

        // Revert reverse_stacks
        for (int i = 0; i < num_of_stacks; i++) {
            j_max = strlen(reverse_stacks[i]);
            for (int j = 0; j < j_max; j++) {
                stacks[i][j] = reverse_stacks[i][j_max-j-1];
            }
        }

        if (line[0] == '\n') {
            // Initialization phase ends here
            break;
        }
    }
    printf("Initial stacks:\n");
    for(int i = 0; i < num_of_stacks; i++) {
        printf("stack %d: %s\n", i+1, stacks[i]);
    }

    // Apply tasks
    while (fgets(task, MAX, file_ptr) != NULL) {
        apply_task(stacks, task);
    }

    // Printf Results
    printf("\nResulting stacks:\n");
    for(int i = 0; i < num_of_stacks; i++) {
        printf("stack %d: %s\n", i+1, stacks[i]);
    }
    printf("\nFinal result:\n");
    for (int i = 0; i < num_of_stacks; i++) {
        if (strlen(stacks[i]) == 0){
            printf(" ");
        }
        else {
            printf("%c", stacks[i][strlen(stacks[i])-1]);
        }

    }
    printf("\n");
    return 0;
}
