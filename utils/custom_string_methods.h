#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void slice(const char *str, char *result, size_t start, size_t end) {
    // taken from http://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing
    strncpy(result, str + start, end - start);
    return;
}


int count_occurrence(const char *str, char chr) {
    int count = 0;
    for (int i = 0; str[i]; i++) {
        if (str[i] == chr) {
            count++;
        }
    }
    return count;
}


int cast_string_to_integer(const char *string) {
    char *end_ptr;
    return strtol(string, &end_ptr, 10);
}


int cast_char_to_integer(const char chr) {
    char string[2];
    string[0] = chr;
    string[1] = '\0';
    return cast_string_to_integer(string);
}
