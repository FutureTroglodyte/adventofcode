// C program to solve Advent of Code - Day 04 - Problem 1
//
// In how many assignment pairs does one range fully contain the other?
//
// https://adventofcode.com/2022/day/4
//
// Code taken from https://stackoverflow.com/questions/2523467
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 1024

int check_containments(char left_side[MAX], char right_side[MAX]) {

    char left_min[MAX];
    char *left_max_ptr;
    char right_min[MAX];
    char *right_max_ptr;

    char *end_ptr;  // for string to integer casting

    // split left_side into two by '-'
    memset(left_min, '\0', sizeof(left_min));
    strcpy(left_min, left_side);
    strtok_r(left_min, "-", &left_max_ptr);

    // split right_side into two by '-'
    memset(right_min, '\0', sizeof(right_min));
    strcpy(right_min, right_side);
    strtok_r(right_min, "-", &right_max_ptr);

    // cast substrings into integers
    int a = strtol(left_min, &end_ptr, 10);
    int b = strtol(left_max_ptr, &end_ptr, 10);
    int x = strtol(right_min, &end_ptr, 10);
    int y = strtol(right_max_ptr, &end_ptr, 10);

    // check for containments
    if ( a <= x && b >= y ) {
        return 1;
    }
    else if ( a >= x && b <= y ) {
        return 1;
    }
    else {
        return 0;
    }
}


int main() {
    char line[MAX];
    FILE *file_ptr = fopen("day04/input.txt", "r");

    char left_side[MAX];
    char *right_side_ptr;

    int final_score = 0;

    int counter = 0;
    while (fgets(line, MAX, file_ptr) != NULL) {

        // split strings into two by ','
        memset(left_side, '\0', sizeof(left_side));
        strcpy(left_side, line);
        strtok_r(left_side, ",", &right_side_ptr);

        // get rid of the last character '\n'
        right_side_ptr[strlen(right_side_ptr)-1] = 0;

        final_score += check_containments(left_side, right_side_ptr);


        printf (
            "%d:  '%s'  '%s' = %d\n",
            counter,
            left_side, 
            right_side_ptr,
            check_containments(left_side, right_side_ptr)
        );
        counter++;
    }

    printf("\nFinal Score is %d\n", final_score);

    return 0;
}

