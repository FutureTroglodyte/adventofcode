// C program to solve Advent of Code - Day 06 - Problem 2
//
// How many characters need to be processed before the first start-of-message marker is detected?
//
// https://adventofcode.com/2022/day/6
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRING_LENGTH 8192
#define SUBSTRING_LENGTH 14


void slice(const char *str, char *result, size_t start, size_t end) {
    // taken from http://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing
    strncpy(result, str + start, end - start);
}


int check_string_for_uniqueness(const char *string) {
    for (int i = 0; i < strlen(string)-1; i++) {
        for (int j = i+1; j < strlen(string); j++) {
            if (string[i] == string[j]) {
                return 0;
            }
        }
    }
    return 1;
}


int main() {
    char line[MAX_STRING_LENGTH];
    FILE *file_ptr = fopen("day06/input.txt", "r");

    char *substring_ptr = (char *)calloc(4, sizeof(char));
    int found_unique_substring;

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {
        found_unique_substring = 0;
        int line_length = strlen(line);
        for (int i = 0; i < line_length-SUBSTRING_LENGTH; i++) {
            if (found_unique_substring == 0) {
                slice(line, substring_ptr, i, i+SUBSTRING_LENGTH);
                if (check_string_for_uniqueness(substring_ptr) == 1) {
                    printf("i = %d - substring = '%s'\n", i+SUBSTRING_LENGTH, substring_ptr);
                    break;
                }
            }
        }
    }
    free(substring_ptr);
    return 0;
}
