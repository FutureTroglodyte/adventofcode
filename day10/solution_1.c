/*
C program to solve Advent of Code - Day 10 - Problem 1

What is the sum of these six signal strengths?

https://adventofcode.com/2022/day/10

Took some Ideas from:
 -

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
// #include "utils.h"

#define INPUT_FILE "day10/input.txt"
// #define INPUT_FILE "day10/test_input.txt"
#define MAX_STRING_LENGTH 32

const int CYCLES_OF_INTEREST[] = {20, 60, 100, 140, 180, 220};


int evaluate_signal_strength(int cycle, int register_value) {
    int length = sizeof(CYCLES_OF_INTEREST) / sizeof(int);
    int signal_strength = 0;

    for (int i = 0; i < length; i++) {
        if (cycle == CYCLES_OF_INTEREST[i]) {
            signal_strength = cycle * register_value;
        }
    }

    return signal_strength;
}

int main(void) {

    FILE *file_ptr = fopen(INPUT_FILE, "r");
    char *line = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    char *command = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    char *register_increase_chr = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    int register_increase = 0;

    int cycle = 0;
    int register_value = 1;

    int total_signal_strength = 0;

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {

        if (strcmp(line, "noop\n") == 0) {
            cycle++;
            total_signal_strength += evaluate_signal_strength(cycle, register_value);
        }
        else {
            cycle++;
            total_signal_strength += evaluate_signal_strength(cycle, register_value);
            cycle++;
            total_signal_strength += evaluate_signal_strength(cycle, register_value);

            /* split line into command and register_increase_chr by ' ' */
            strcpy(command, line);
            strtok_r(command, " ", &register_increase_chr);
            register_increase_chr[strlen(register_increase_chr) - 1] = '\0';
            register_increase = cast_string_to_integer(register_increase_chr);

            register_value += register_increase;
        }
    }

    fclose(file_ptr);
    free(line);
    free(command);

    printf("Resulting Total Signal Strength: %d\n", total_signal_strength);

    return 0;
}
