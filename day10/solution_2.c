/*
C program to solve Advent of Code - Day 10 - Problem 2

What eight capital letters appear on your CRT?

https://adventofcode.com/2022/day/10

---

Resulting Image:

  #### #### ###  ###  ###  #### #### ####
  #       # #  # #  # #  # #       # #
  ###    #  ###  #  # ###  ###    #  ###
  #     #   #  # ###  #  # #     #   #
  #    #    #  # #    #  # #    #    #
  #    #### ###  #    ###  #    #### #

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
// #include "utils.h"

#define INPUT_FILE "day10/input.txt"
// #define INPUT_FILE "day10/test_input.txt"
#define MAX_STRING_LENGTH 32
#define MAX_CYCLES 240
#define PIXELS_PER_ROW 40


void draw_pixel(int cycle, int sprite_position, char *image) {
    if (abs((cycle % PIXELS_PER_ROW) - sprite_position) <= 1) {
        image[cycle] = '#';
    }
    else {
        image[cycle] = ' ';
    }

    return;
}


void display_image(const char *image) {

    size_t start = 0;
    size_t end = 0;

    char *image_row = (char*) calloc(PIXELS_PER_ROW+1, sizeof(char));

    for (int row = 0; PIXELS_PER_ROW*row < MAX_CYCLES; row++) {
        start = PIXELS_PER_ROW * row;
        end = PIXELS_PER_ROW * (row + 1);
        slice(image, image_row, start, end);
        printf("  %s\n", image_row);
    }
    printf("\n");

    free(image_row);

    return;
}

int main(void) {

    FILE *file_ptr = fopen(INPUT_FILE, "r");
    char *line = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    char *command = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    char *register_increase_chr = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    int register_increase = 0;

    int cycle = 0;
    char *image = (char*) calloc(MAX_CYCLES, sizeof(char));

    int sprite_position = 1;

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {

        if (cycle >= MAX_CYCLES) {
            break;
        }

        if (strcmp(line, "noop\n") == 0) {
            draw_pixel(cycle, sprite_position, image);
            cycle++;
        }
        else {
            draw_pixel(cycle, sprite_position, image);
            cycle++;
            draw_pixel(cycle, sprite_position, image);
            cycle++;

            /* split line into command and register_increase_chr by ' ' */
            strcpy(command, line);
            strtok_r(command, " ", &register_increase_chr);
            register_increase_chr[strlen(register_increase_chr) - 1] = '\0';
            register_increase = cast_string_to_integer(register_increase_chr);

            sprite_position += register_increase;
        }
    }
    fclose(file_ptr);
    free(line);
    free(command);

    printf("Resulting Image:\n\n");

    display_image(image);

    free(image);

    return 0;
}
