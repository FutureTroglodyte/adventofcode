// C program to solve Advent of Code - Day 03 - Problem 2
//
// What is the sum of the priorities of those item types?
//
// https://adventofcode.com/2022/day/3
//

#include <stdio.h>
#include <string.h>

#define MAX 1024


void slice(const char *str, char *result, size_t start, size_t end) {
    // taken from http://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing
    strncpy(result, str + start, end - start);
}

char find_common_item(
    char first_pack[MAX],
    char second_pack[MAX],
    char third_pack[MAX]
) {
    for (int i = 0; i < strlen(first_pack); i++) {
        //printf("\n%d - %c", i, first_pack[i]);
        for (int j = 0; j < strlen(second_pack); j++) {
            for (int k = 0; k < strlen(third_pack); k++) {
                if (
                    first_pack[i] == second_pack[j] &&
                    second_pack[j] == third_pack[k]
                ) {
                    return first_pack[i];
                }
            }
        }
    }
}


int get_priority(char c) {
    int priority = (int)(c)-96;
    if (priority < 1) {
        priority += 58;
    }
    return priority;
}


int main() {
    char string[MAX];
    FILE *fp = fopen("day03/input.txt", "r");

    int string_length;
    int counter = 0;
    char first_pack[MAX];
    char second_pack[MAX];
    char third_pack[MAX];
    char common_item;
    int priority;
    int total_priority = 0;

    while (fgets(string, MAX, fp) != NULL) {
        if (counter % 3 == 0) {
            //memset(first_pack, '\0', sizeof(first_pack));
            strcpy(first_pack, string);
        }
        else if (counter % 3 == 1) {
            //memset(second_pack, '\0', sizeof(second_pack));
            strcpy(second_pack, string);
        }
        else {
            //memset(third_pack, '\0', sizeof(third_pack));
            strcpy(third_pack, string);

            common_item = find_common_item(first_pack, second_pack, third_pack);
            priority = get_priority(common_item);
            printf("%c - %d\n", common_item, priority);
            total_priority += priority;
        }
        counter += 1;
    }
    printf("%d\n", total_priority);
}
