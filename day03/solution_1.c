// C program to solve Advent of Code - Day 03 - Problem 1
//
// What is the sum of the priorities of those item types?
//
// https://adventofcode.com/2022/day/3
//

#include <stdio.h>
#include <string.h>

#define MAX 1024


void slice(const char *str, char *result, size_t start, size_t end) {
    // taken from http://stackoverflow.com/questions/26620388/c-substrings-c-string-slicing
    strncpy(result, str + start, end - start);
}

char find_common_item(char first_part[MAX], char second_part[MAX]) {
    for (int i = 0; i < strlen(first_part); i++) {
        //printf("\n%d - %c", i, first_part[i]);
        for (int j = 0; j < strlen(second_part); j++) {
            if (first_part[i] == second_part[j]) {
                return first_part[i];
            }
        }
    }
}


int get_priority(char c) {
    int priority = (int)(c)-96;
    if (priority < 1) {
        priority += 58;
    }
    return priority;
}


int main() {
    char string[MAX];
    FILE *fp = fopen("day03/input.txt", "r");

    int string_length;
    char first_part[MAX];
    char second_part[MAX];
    char common_item;
    int priority;
    int total_priority = 0;

    while (fgets(string, MAX, fp) != NULL) {
        string_length = strlen(string);

        // printf("%d - ", string_length);
        // printf("%s", string);


        // Split string into first_part and second_part
        memset(first_part, '\0', sizeof(first_part));
        slice(string, first_part, 0, (string_length-1)/2);

        memset(second_part, '\0', sizeof(second_part));
        slice(string, second_part, (string_length-1)/2, string_length-1);

        // printf("- %s  -  %s\n", first_part, second_part);

        common_item = find_common_item(first_part, second_part);
        priority = get_priority(common_item);
        // printf("%c - %d\n", common_item, priority);
        total_priority += priority;
    }
    printf("%d\n", total_priority);
    return 0;
}
