# Advent of Code

## 2022

Learning *C* base language by solving [Advent of Code 2022](https://adventofcode.com/2022) Problems. There are 50 Problems in total, two for each day from 2022-12-01 until 2022-12-25.

![AoC_2022-12-19](images/AoC_2022-12-19.png)

The Code was compiled with *gcc (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0* and debugged with *GNU gdb (Ubuntu 12.1-0ubuntu1~22.04) 12.1*.


## Setup


### Cmake

- [apt repository](https://apt.kitware.com/)
