/*
C program to solve Advent of Code - Day 11 - Problem 2

What is the level of monkey business after 10000 rounds?

https://adventofcode.com/2022/day/11

Ideas from:
  - https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm

Took a hint from:
  - https://github.com/codeman869/AdventOfCode2022/blob/main/Day%2011/main.c
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
// #include "utils.h"

#define INPUT_FILE "day11/input.txt"
// #define INPUT_FILE "day11/test_input.txt"
#define MAX_STRING_LENGTH 128
#define MAX_ITEMS 128
#define MAX_MONKEYS 10
#define NUM_OF_ROUNDS 10000


/* --- Structs & Enums --- */


typedef enum Operator {
    ADD,
    MUL,
    POW
} Operator;

typedef struct Operation {
    Operator operator;
    int value;
} Operation;

typedef struct Test {
    int condition_divisible_by;
    int if_true_throw_to_monkey;
    int if_false_throw_to_monkey;
} Test;

typedef struct Monkey {
    unsigned long items[MAX_ITEMS];
    int number_of_items;
    Operation operation;
    Test test;
    int num_of_inspections;
} Monkey;


/* --- Util Functions --- */


int parse_starting_items(Monkey *monkeys, int monkeys_count, const char *line) {
    char *line_cpy = strdup(line); 
    char *token;
    const char delimiter[4] = " :,";
    int i = 0;

   
    /* get the first token */
    token = strtok(line_cpy, delimiter);
    token = strtok(NULL, delimiter);
    token = strtok(NULL, delimiter);

    /* walk through other tokens */
    while( token != NULL ) {
        printf("%s ", token);
        monkeys[monkeys_count].items[i] = cast_string_to_integer(token);
        token = strtok(NULL, delimiter);
        i++;
    }

    monkeys[monkeys_count].number_of_items = i;

    return 0;
}


int parse_operation(Monkey *monkeys, int monkeys_count, char *line) {
    char *line_cpy = strdup(line);
    char operator;
    char *value = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    /* line is i.e. 'Operation: new = old * 19' */

    strsep(&line_cpy," ");  // = ' '
    strsep(&line_cpy," ");  // = ' '
    strsep(&line_cpy," ");  // = 'Operation:'
    strsep(&line_cpy," ");  // = 'new'
    strsep(&line_cpy," ");  // = '='
    strsep(&line_cpy," ");  // = 'old'
    operator = strsep(&line_cpy," ")[0];
    strcpy(value, strsep(&line_cpy," "));

    monkeys[monkeys_count].operation.value = cast_string_to_integer(value);

    if (
        operator == '*' &&
        strncmp(value, "old", strlen("old")) == 0
    ) {
        monkeys[monkeys_count].operation.operator = POW;
    } 
    else if (operator == '*') {
        monkeys[monkeys_count].operation.operator = MUL;
    }
    else if (operator == '+') {
        monkeys[monkeys_count].operation.operator = ADD;
    }
    else {
        perror("Error: Operation can't be parsed!\n");
        exit(1);
    }

    free(value);

    return 0;
}


unsigned long apply_operation(const Operation operation, const unsigned long old) {
    unsigned long new;

    if (operation.operator == ADD) {
        new = old + operation.value;
    }
    else if (operation.operator == MUL) {
        new = old * operation.value;
    }
    else if (operation.operator == POW) {
        new = old * old;
    }
    else {
        perror("Error: Operation has unknown operator!\n");
        exit(1);
    }
    return new;
}


int apply_test(const Test test, const unsigned long item) {
    if (item % test.condition_divisible_by == 0) {
        return test.if_true_throw_to_monkey;
    }
    else {
        return test.if_false_throw_to_monkey;
    }
}


int play_round(Monkey *monkeys, const int monkeys_count, int shared_modulus, int feedback) {

    unsigned long item = 0;
    int target = -1;

    for (int i = 0; i <= monkeys_count; i++) {
        /* Handle Monkey i */
        for (int j = 0; j < monkeys[i].number_of_items; j++) {
            /* Inspect item j. 'Worry Level' (= item) is evaluated via monkey.operation */
            item = monkeys[i].items[j];
            item = apply_operation(monkeys[i].operation, item);
            /* throw item to target monkey. The target for item is found via monkey.test */
            target = apply_test(monkeys[i].test, item);
            item = item % shared_modulus;
            monkeys[target].items[monkeys[target].number_of_items] = item;
            monkeys[target].number_of_items++;
            monkeys[i].items[j] = 0;
            /* Raise m*/
            monkeys[i].num_of_inspections++;
        }
        monkeys[i].number_of_items=0;
    }

    if (feedback == 1) {
        for (int i = 0; i <= monkeys_count; i++) {
            printf("Monkey %d: ", i);
            for (int j = 0; j < monkeys[i].number_of_items; j++) {
                printf("%ld ", monkeys[i].items[j]);
            }
            printf("\n");
        }
    }


    // item_ptr = &monkeys[0].items[0];
    // monkeys[0].items[2] = *item_ptr;

    return 0;
}


/* --- Main Function --- */


int main(void) {

    FILE *file_ptr = fopen(INPUT_FILE, "r");
    char *line = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    Monkey *monkeys = (Monkey*)calloc(MAX_MONKEYS, sizeof(Monkey));
    int monkeys_count = -1;

    char line_cpy[MAX_STRING_LENGTH];
    char *tail = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    int shared_modulus = 1;

    int rounds_count = 0;
    unsigned long max_num_of_inspections[2] = {0, 0};


    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {
        strcpy(line_cpy, line);

        if (strncmp(line, "Monkey", strlen("Monkey")) == 0) {
            printf("\nMonkey found: %d\n", monkeys_count+1);
            monkeys_count++;
        }
        else if (strncmp(line, "  Starting items:", strlen("  Starting items:")) == 0) {
            printf("Starting items found: ");
            parse_starting_items(monkeys, monkeys_count, line);
            // printf("\n");
        }
        else if (strncmp(line, "  Operation:", strlen("  Operation:")) == 0) {
            printf("Operation found\n");
            parse_operation(monkeys, monkeys_count, line);
        }
        else if (strncmp(line, "  Test:", strlen("  Test:")) == 0) {
            printf("Test found\n");
            //memset(tail, '\0', sizeof(tail));
            tail = strrchr(line_cpy, ' ');
            tail[strlen(tail)-1] = '\0';
            monkeys[monkeys_count].test.condition_divisible_by = cast_string_to_integer(tail);

        }
        else if (strncmp(line, "    If true:", strlen("    If true:")) == 0) {
            printf("If True found\n");
            // memset(tail, '\0', sizeof(tail));
            tail = strrchr(line_cpy, ' ');
            tail[strlen(tail)-1] = '\0';
            monkeys[monkeys_count].test.if_true_throw_to_monkey = cast_string_to_integer(tail);
        }
        else if (strncmp(line, "    If false:", strlen("    If false:")) == 0) {
            printf("If False found\n");
            // memset(tail, '\0', sizeof(tail));
            tail = strrchr(line_cpy, ' ');
            tail[strlen(tail)-1] = '\0';
            monkeys[monkeys_count].test.if_false_throw_to_monkey = cast_string_to_integer(tail);
        }
    }

    fclose(file_ptr);


    /* Evaluate shared modulus */
    for(int i = 0; i <= monkeys_count; i++) {
        shared_modulus *= monkeys[i].test.condition_divisible_by;
    }

    /* Play some Rounds */
    while (rounds_count < NUM_OF_ROUNDS) {
        // printf("\n--- Round %d ---\n", rounds_count+1);
        play_round(monkeys, monkeys_count, shared_modulus, 0);
        rounds_count++;
    }

    /* Evaluate Results */
    printf("\n--- Results ---\n");
    for (int i = 0; i <= monkeys_count; i++) {
        printf("Monkey %d' number of inspections: %d\n", i, monkeys[i].num_of_inspections);
        if (max_num_of_inspections[0] < monkeys[i].num_of_inspections) {
            max_num_of_inspections[1] = max_num_of_inspections[0];
            max_num_of_inspections[0] = monkeys[i].num_of_inspections;
        }
        else if (max_num_of_inspections[1] < monkeys[i].num_of_inspections) {
            max_num_of_inspections[1] = monkeys[i].num_of_inspections;
        }
    }

    printf("\nResulting max numbers of inspections: %ld %ld\n", max_num_of_inspections[0], max_num_of_inspections[1]);

    printf("\nResulting Level of monkey business: %ld\n\n", max_num_of_inspections[0] * max_num_of_inspections[1]);

    free(line);
    free(monkeys);
    //free(tail);

    return 0;
}
