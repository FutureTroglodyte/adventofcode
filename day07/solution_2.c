/*
C program to solve Advent of Code - Day 07 - Problem 2

Find the smallest directory that, if deleted, would free up enough space on the filesystem
to run the update. What is the total size of that directory?

https://adventofcode.com/2022/day/7


Took useful ideas from here: https://overiq.com/c-programming-101/array-of-structures-in-c/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"

#define INPUT_FILE "day07/input.txt"
// #define INPUT_FILE "day07/test_input.txt"
// #define INPUT_FILE "day07/test_input_2.txt"
#define MAX_STRING_LENGTH 8192
#define MAX_DIRECTORIES 512
#define SIZE_LIMIT 100000

int directories_added = 0;

/* Structs */


typedef struct directory {
    char path[MAX_STRING_LENGTH];
    long int size;
} Directory;


/* Utils */


int directory_exists(Directory *filesystem, const char *path, int directories_added) {
    // TODO: Obsolete?
    for (int i = 0; i <= directories_added; i++) {
        if (strcmp(filesystem[i].path, path) == 0) {
            return 1;
        }
    }
    return 0;
}


int find_directory(Directory *filesystem, const char *path, int directories_added) {
    for(int i = 0; i <= directories_added; i++) {
        if (strcmp(filesystem[i].path, path) == 0) {
            return i;
        }
    }
    printf("'%s' could not be found! Just found:\n", path);
    for(int i = 0; i <= directories_added; i++) {
        printf("- '%s'\n", filesystem[i].path);
    }
    // TODO: Exit here (Throw Error)
    return -1;
}


int add_directory(
    Directory *filesystem,
    const char *path,
    int directories_added
) {
    strcpy(filesystem[directories_added].path, path);
    return 0;
}


int sum_file_sizes_in_ls_call(
    Directory *filesystem,
    const char *path,
    int directories_added,
    const char *ls_call
) {
    char *line_ptr = strdup(ls_call);  // for string splitting
    char parent_path[MAX_STRING_LENGTH];  // for adding file_size to all parent directories
    
    char ls_call_left[MAX_STRING_LENGTH];

    char *end_ptr;  // for string to integer casting
    
    long int file_size = 0;
    int index;
    int end;  // for generating parent_path
    int safety_break = 0;

    /* Split ls_call by ' ' */
    strcpy(ls_call_left, strsep(&line_ptr, " "));

    if (strcmp(ls_call_left, "dir") != 0) {
        file_size = strtol(ls_call_left, &end_ptr, 10);  // cast str to int

        memset(parent_path, '\0', MAX_STRING_LENGTH);
        strcpy(parent_path, path);

        index = find_directory(filesystem, parent_path, directories_added);
        filesystem[index].size += file_size;
        while (strlen(parent_path) > 1) {

            /* Reduce */
            parent_path[strlen(parent_path) -1] = '\0';
            end = strlen(parent_path)-strlen(strrchr(parent_path, '/')) + 1;
            parent_path[end] = '\0';

            if (directory_exists(filesystem, parent_path, directories_added) == 0) {
                add_directory(filesystem, parent_path, directories_added);
                directories_added++;
            }

            index = find_directory(filesystem, parent_path, directories_added);
            filesystem[index].size += file_size;

            safety_break++;
            if (safety_break >= MAX_DIRECTORIES) {
                break;
            }
        }
    }
    return 0;
}


/* Main Function */


int main() {
    char line[MAX_STRING_LENGTH];
    int line_length = 0;
    FILE *file_ptr = fopen(INPUT_FILE, "r");

    Directory *filesystem = (Directory*) calloc(MAX_DIRECTORIES, sizeof(Directory));

    char *call = (char*) calloc(5, sizeof(char));
    char *path = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    char *dir_name = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    // char *parent_path = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    int end;

    int ls_called_flag = 0;
    int index = -1;

    int space_to_free = 0;
    long int size_of_smallest_directory_that_serves_purpose;

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {
        line_length = strlen(line);

        /* inspect call ('$ cd' or '$ ls') */
        memset(call, '\0', 5);
        slice(line, call, 0, 4);

        if (call[0] == '$' && call[2] == 'c' && call[3] == 'd') { /* cd */
            ls_called_flag = 0;

            /* Extract dir_name (and path) of cd call */
            memset(dir_name, '\0', MAX_STRING_LENGTH);
            slice(line, dir_name, 5, line_length-1);

            if (dir_name[0] == '.' && dir_name[1] == '.') {  /* goes up case */
                

                /* Reduce: If path is 'a/b/c/', then super is '/a/b/' */
                path[strlen(path) -1] = '\0';
                end = strlen(path)-strlen(strrchr(path, '/')) + 1;
                path[end] = '\0';

                if (path[strlen(path)-1] != '/') {
                    strcat(path, "/\0");  /* add a trailing '/' if needed */
                }
            }
            else {  /* goes down case */
                strcat(path, dir_name);  /* concat path and dir name */
                if (path[strlen(path)-1] != '/') {
                    strcat(path, "/\0");  /* add a trailing '/' if needed */
                }
                if (directory_exists(filesystem, path, directories_added) == 0) {
                    add_directory( filesystem, path, directories_added);
                    directories_added++;
                }
            }
        }
        else if (call[0] == '$' && call[2] == 'l' && call[3] == 's') { /* ls */
            ls_called_flag = 1;
        }
        else{ /* should be output after ls call */
            if (ls_called_flag == 1) {
                sum_file_sizes_in_ls_call(
                    filesystem, path, directories_added, line
                );
            }
        }
    }

    space_to_free = 30000000 - 70000000 + filesystem[0].size;
    size_of_smallest_directory_that_serves_purpose = filesystem[0].size;

    printf("\nResulting filesystem:\n");
    for (int i = 0; i < directories_added; i++) {
        printf(
            "name '...' - size %ld\n",
            // filesystem[i].path,
            filesystem[i].size
        );
        if (filesystem[i].size <= size_of_smallest_directory_that_serves_purpose) {
            if (filesystem[i].size >= space_to_free) {
                size_of_smallest_directory_that_serves_purpose = filesystem[i].size;
            }
        }
    }

    printf(
        "\nNumber of directories in filesystem:\n%d\n",
        directories_added
    );

    printf(
        "\nResulting size_of_smallest_directory_that_serves_purpose:\n%ld\n",
        size_of_smallest_directory_that_serves_purpose
    );

    /* free your stuff */
    free(path);
    free(call);

    return 0;
}
