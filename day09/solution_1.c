/*
C program to solve Advent of Code - Day 09 - Problem 1

How many positions does the tail [T] of the rope visit at least once?

https://adventofcode.com/2022/day/9

Took some Ideas from:
 - https://www.delftstack.com/howto/c/c-max-and-min-function/

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
// #include "utils.h"

#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

// #define INPUT_FILE "day09/input.txt"
#define INPUT_FILE "day09/input.txt"
#define MAX_STRING_LENGTH 32
#define MAX_NUM_OF_VISITED_POINTS 65536


typedef struct point {
    int x;
    int y;
} Point;


int max_norm(Point P, Point Q) {
    return max(abs(P.x - Q.x), abs(P.y - Q.y));
}


Point make_step(Point H, const char *direction) {
    if (direction[0] == 'R') {
        H.x++;
    }
    else if (direction[0] == 'L') {
        H.x--;
    }
    else if (direction[0] == 'U') {
        H.y++;
    }
    else if (direction[0] == 'D') {
        H.y--;
    }
    else {
        perror("What happened?");
    }

    return H;
}


Point follow(Point T, const Point H) {

    if (max_norm(T, H) > 1) {
        if (H.x > T.x) {
            T.x++;
        }
        if (H.x < T.x) {
            T.x--;
        }
        if (H.y > T.y) {
            T.y++;
        }
        if (H.y < T.y) {
            T.y--;
        }
    }

    return T;
}


int add_to_unique_visited_points(Point P, Point *visited_points, int visited_points_counter) {

    for (int i = 0; i < visited_points_counter; i++) {
        if (visited_points[i].x == P.x && visited_points[i].y == P.y) {
            return visited_points_counter;
        }
    }
    visited_points[visited_points_counter] = P;
    visited_points_counter++;
    return visited_points_counter;
}


int main(void) {

    FILE *file_ptr = fopen(INPUT_FILE, "r");
    char *line = (char*) calloc(MAX_STRING_LENGTH, sizeof(char)); 

    char *direction = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    char *steps_chr = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));
    int steps = 0;

    Point H = { 0 };
    Point T = { 0 };
    Point *unique_visited_points = (Point*) calloc(MAX_NUM_OF_VISITED_POINTS, sizeof(Point));
    int visited_points_counter = 0;

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {

        /* split line into direction and steps_chr by ' ' */
        strcpy(direction, line);
        strtok_r(direction, " ", &steps_chr);
        steps_chr[strlen(steps_chr) - 1] = '\0';
        steps = cast_string_to_integer(steps_chr);

        for (int step = 0; step < steps; step++) {
            H = make_step(H, direction);
            T = follow(T, H);
            visited_points_counter = add_to_unique_visited_points(
                T,
                unique_visited_points,
                visited_points_counter
            );
        }
    }

    printf("Resulting Visited Points Count: %d\n", visited_points_counter);

    return 0;
}
