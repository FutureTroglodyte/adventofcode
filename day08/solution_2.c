/*
C program to solve Advent of Code - Day 08 - Problem 2

What is the highest scenic score possible for any tree?

https://adventofcode.com/2022/day/8

Took some Ideas from:
 - https://stackoverflow.com/questions/32998105/reading-a-2d-array-from-a-file-in-c

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
#include "utils.h"

#define INPUT_FILE "day08/input.txt"
// #define INPUT_FILE "day08/test_input.txt"
#define MAX_ROWS 128
#define MAX_COLUMNS 128

int main(void){

    FILE* file_ptr;
    char line[MAX_COLUMNS];

    int rows = 0;
    int columns = 0;
    char c;
    char char_array[MAX_ROWS][MAX_COLUMNS] = { '\0' };
    /* Hint: int array[rows][columns] is declared below */

    int view_distance = 0;
    int scenic_score = 1;
    int max_scenic_score = 0;

    /* Evaluate number of rows & columns */

    if ((file_ptr = fopen(INPUT_FILE, "r")) == NULL){
        exit(1);
    }

    while (fgets(line, MAX_COLUMNS, file_ptr) != NULL) {
        if (columns < strlen(line)) {
            columns = strlen(line);
        }
        rows++;
    }

    fclose(file_ptr);

    /* Read file as 2D char array */

    if ((file_ptr = fopen(INPUT_FILE, "r")) == NULL){
        exit(1);
    }

    for (int i = 0; i < rows; i++) {
        for(int j = 0; j < columns; j++) {
            c = fgetc(file_ptr);
            if (c == '\n') {
                break;
            }
            char_array[i][j] = c;
        }
    }

    fclose(file_ptr);

    /* Cast 2D char array to 2D integer array */

    columns--;
    int array[rows][columns];

    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < columns; j++) {
            array[i][j] = cast_char_to_integer(char_array[i][j]);
        }
    }

    /* Solve Problem 2 */

    for(int i = 1; i < rows - 1; i++) {
        for(int j = 1; j < columns - 1; j++) {

            scenic_score = 1;

            view_distance = 1;
            for (int k = i-1; k > 0; k--) {
                if(array[k][j] >= array[i][j]) {
                    break;
                }
                view_distance++;
            }
            scenic_score *= view_distance;

            view_distance = 1;
            for (int k = i+1; k < rows-1; k++) {
                if(array[k][j] >= array[i][j]) {
                    break;
                }
                view_distance++;
            }
            scenic_score *= view_distance;

            view_distance = 1;
            for (int l = j-1; l > 0; l--) {
                if(array[i][l] >= array[i][j]) {
                    break;
                }
                view_distance++;
            }
            scenic_score *= view_distance;

            view_distance = 1;
            for (int l = j+1; l < columns-1; l++) {
                if(array[i][l] >= array[i][j]) {
                    break;
                }
                view_distance++;
            }
            scenic_score *= view_distance;

            if (max_scenic_score < scenic_score) {
                max_scenic_score = scenic_score;
            }
        }
    }

    printf("Solution 2 - Max Scenic Score: %d\n", max_scenic_score);

    return 0;
}
