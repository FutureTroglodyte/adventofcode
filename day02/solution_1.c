// C program to solve Advent of Code - Day 02 - Problem 1
//
// What would your total score be if everything
// goes exactly according to your strategy guide?
//
// https://adventofcode.com/2022/day/2
//
// It is:
//  - A, X: Rock
//  - B, Y: Paper
//  - C, Z: Scissors

#include <stdio.h>
#include <string.h>

#define MAX 8


int evaluate_round(char opponents_choice, char my_choice) {

    int score;

    if (my_choice == 'X') {
        score = 1;
        if (opponents_choice == 'A') {
            // game drawn
            score += 3;
        }
        else if (opponents_choice == 'B') {
            // game lost
            score += 0;
        }
        else if (opponents_choice == 'C') {
            // game won
            score += 6;
        }
    }
    else if (my_choice == 'Y') {
        score = 2;
        if (opponents_choice == 'A') {
            // game won
            score += 6;
        }
        else if (opponents_choice == 'B') {
            // game drawn
            score += 3;
        }
        else if (opponents_choice == 'C') {
            // game lost
            score += 0;
        }
    }
    else if (my_choice == 'Z') {
        score = 3;
        if (opponents_choice == 'A') {
            // game lost
            score += 0;
        }
        else if (opponents_choice == 'B') {
            // game won
            score += 6;
        }
        else if (opponents_choice == 'C') {
            // game drawn
            score += 3;
        }
    }

    return score;
}



int main() {
    char string[MAX];
    FILE *fp = fopen("day02/input.txt", "r");

    char opponents_choice;
    char my_choice;
    int total_score = 0;

    while (fgets(string, MAX, fp) != NULL) {

        opponents_choice = string[0];
        my_choice = string[2];
        total_score += evaluate_round(opponents_choice, my_choice);
        // printf("Score: %d\n", evaluate_round(opponents_choice, my_choice));
    }

    printf("Total score: %d\n", total_score);
}