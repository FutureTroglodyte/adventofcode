// C program to solve Advent of Code - Day 02 - Problem 1
//
// What would your total score be if everything
// goes exactly according to your strategy guide?
//
// https://adventofcode.com/2022/day/2
//
// It is:
//  - A, X: Rock
//  - B, Y: Paper
//  - C, Z: Scissors

#include <stdio.h>
#include <string.h>

#define MAX 8

char make_my_choice(char opponents_choice, char how_should_round_end) {

    char my_choice;

    if (how_should_round_end == 'X') {
        // round should be lost
        if (opponents_choice == 'A') {
            my_choice = 'Z';
        }
        else if (opponents_choice == 'B') {
            my_choice = 'X';
        }
        else if (opponents_choice == 'C') {
            my_choice = 'Y';
        }
    }
    else if (how_should_round_end == 'Y') {
        // round should be drawn
        if (opponents_choice == 'A') {
            my_choice = 'X';
        }
        else if (opponents_choice == 'B') {
            my_choice = 'Y';
        }
        else if (opponents_choice == 'C') {
            my_choice = 'Z';
        }
    }
    else if (how_should_round_end == 'Z') {
        // round should by won
        if (opponents_choice == 'A') {
            my_choice = 'Y';
        }
        else if (opponents_choice == 'B') {
            my_choice = 'Z';
        }
        else if (opponents_choice == 'C') {
            my_choice = 'X';
        }
    }

    return my_choice;
}


int evaluate_round(char opponents_choice, char my_choice) {

    int score;

    if (my_choice == 'X') {
        score = 1;
        if (opponents_choice == 'A') {
            // round drawn
            score += 3;
        }
        else if (opponents_choice == 'B') {
            // round lost
            score += 0;
        }
        else if (opponents_choice == 'C') {
            // round won
            score += 6;
        }
    }
    else if (my_choice == 'Y') {
        score = 2;
        if (opponents_choice == 'A') {
            // round won
            score += 6;
        }
        else if (opponents_choice == 'B') {
            // round drawn
            score += 3;
        }
        else if (opponents_choice == 'C') {
            // round lost
            score += 0;
        }
    }
    else if (my_choice == 'Z') {
        score = 3;
        if (opponents_choice == 'A') {
            // round lost
            score += 0;
        }
        else if (opponents_choice == 'B') {
            // round won
            score += 6;
        }
        else if (opponents_choice == 'C') {
            // round drawn
            score += 3;
        }
    }

    return score;
}



int main() {
    char string[MAX];
    FILE *fp = fopen("day02/input.txt", "r");

    char opponents_choice;
    char how_should_round_end;
    char my_choice;
    int total_score = 0;

    while (fgets(string, MAX, fp) != NULL) {

        opponents_choice = string[0];
        how_should_round_end = string[2];

        my_choice = make_my_choice(opponents_choice, how_should_round_end);
        // printf("%c - %c\n", opponents_choice, my_choice);

        total_score += evaluate_round(opponents_choice, my_choice);
        // printf("Score: %d\n", evaluate_round(opponents_choice, my_choice));
    }

    printf("Total score: %d\n", total_score);
}