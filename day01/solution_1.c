// C program to solve Advent of Code - Day 01 - Problem 1
//
// Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?
//
// https://adventofcode.com/2022/day/1
//

#include <stdio.h>
#include <stdlib.h>

#define MAX 8

int main()
{
	char string[MAX];
	FILE* fp = fopen("day01/input.txt", "r");
	char *endptr;
	
	int sum = 0;
	long max = 0;

	while(fgets(string, MAX, fp)) {

		if (string[0] == '\n') {  // damn, this took way too long...
			// printf("\nstop here!\n\n");
			sum = 0;
		}
		else {
			// printf("%s", string);
			sum += strtol(string, &endptr, 10);
		}
		if (max < sum) {
			max = sum;
		}
	}

	printf("The Maximum found is %ld\n", max);

	return 0;
}
