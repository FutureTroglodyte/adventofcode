// C program to solve Advent of Code - Day 01 - Problem 2
//
// Find the top three Elves carrying the most Calories. 
// How many Calories are those Elves carrying in total?
//
// https://adventofcode.com/2022/day/1
//

#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE_LENGTH 8

int main()
{
	char string[MAX_LINE_LENGTH];
	FILE* fp = fopen("day01/input.txt", "r");
	char *endptr;
	
	int sum = 0;
	int top1 = 0;
	int top2 = 0;
	int top3 = 0;

	while(fgets(string, MAX_LINE_LENGTH, fp)) {

		if (string[0] == '\n') {
			sum = 0;
		}
		else {
			sum += strtol(string, &endptr, 10);
		}
		if (top1 < sum) {
			top3 = top2;
			top2 = top1;
			top1 = sum;
		}
		else if (top2 < sum) {
			top3 = top2;
			top2 = sum;
		}
		else if (top3 < sum) {
			top3 = sum;
		}
	}

	printf("top1: %d\n", top1);
	printf("top2: %d\n", top2);
	printf("top3: %d\n", top3);

	printf("\nIn sum that is: %d\n", top1+top2+top3);

	return 0;
}
