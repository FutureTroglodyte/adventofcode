/*
C program to solve Advent of Code - Day TODO - Problem TODO

[...]

https://adventofcode.com/2022/day/TODO

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../utils/custom_string_methods.h"
// #include "utils.h"

#define INPUT_FILE "dayTODO/input.txt"
// #define INPUT_FILE "dayTODO/test_input.txt"
#define MAX_STRING_LENGTH 64



int main(void) {

    FILE *file_ptr = fopen(INPUT_FILE, "r");
    char *line = (char*) calloc(MAX_STRING_LENGTH, sizeof(char));

    while (fgets(line, MAX_STRING_LENGTH, file_ptr) != NULL) {

        
    }
    fclose(file_ptr);

    printf("Resulting [...]:\n\n");

    free(file_ptr);
    free(line);

    return 0;
}
